var http = require('http'),
    httpProxy = require('http-proxy');

// create a proxy server with WS-proxying enabled
var proxy = httpProxy.createProxyServer({
  ws: true
});

// The application port
const PORT = process.env.PORT || 3000;
// The proxy target URL
const TARGET_URL = 'http://104.40.20.254';  // IP of existing quadrant-prod-build VM NIC

// create a simple http server to proxy requests
var server = http.createServer(function(req, res) {
  proxy.web(req, res, {
    target: TARGET_URL
  }, function(e) {
    log_error(e,req);
  });
});

// Upgrade to WS
server.on('upgrade',function(req,res) {
  proxy.ws(req, res, {
    target: TARGET_URL
  }, function(e) {
    log_error(e,req);
  });
});

server.listen(PORT);

// Error logging
function log_error(e, req) {
  if (e) {
    console.error(e.message);
    console.log(req.headers.host, '-->', TARGET_URL);
    console.log('-----');
  }
}
